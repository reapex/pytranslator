import config
import datetime
import os.path

from translate import Translator

chooseFile = input("Choisissez le fichier à traduire : ")
chooseLanguage = input("Choisissez en quelle langue traduire : ")
saveFile = input("Voulez vous enregistrer la traduction dans un fichier ? y/n [no] : ")

if saveFile != "y" and saveFile != "n" and saveFile != "":
    print("Réponse invalide.")
    
else:
    if saveFile == "y":
        if chooseLanguage in config.Locales:
            translator = Translator(to_lang=chooseLanguage)

            if not os.path.exists(chooseFile):
                print("Le fichier n\'existe pas ou n\'a pas été trouvé")
            else:
                openFile = open(chooseFile,"r") 
                writeFile = open("translate.txt","a")

                filesize = os.path.getsize("translate.txt")
                if filesize == 0:
                    writeFile.write("---------------------------------------------")
                    writeFile.write("Translation : " + str(datetime.datetime.now()))
                    writeFile.write("---------------------------------------------\n")
                else:
                    writeFile.write("\n---------------------------------------------")
                    writeFile.write("Translation : " + str(datetime.datetime.now()))
                    writeFile.write("---------------------------------------------\n")

                    for line in openFile: 
                        translation = translator.translate(line)
                        writeFile.write("\n" + translation)
        else:
            print("Language invalide")
    else:
        if chooseLanguage in config.Locales:
            translator = Translator(to_lang=chooseLanguage)
            openFile = open(chooseFile,"r") 
            readFile = openFile.readlines() 

            for line in readFile: 
                translation = translator.translate(line)
                print(translation)

        else:
            print("Language invalide.")